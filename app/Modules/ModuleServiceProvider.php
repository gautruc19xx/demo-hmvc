<?php

namespace App\Modules;

use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Register modules for your application.
     *
     * @return void
     */
    public function register()
    {
        // Config
        $configFile = [
            'demo' => __DIR__.'/Demo/config/demo.php',
        ];
        foreach ($configFile as $alias => $path) {
            $this->mergeConfigFrom($path, $alias);
        }

        // Middleware

        // Command
    }

    /**
     * Boot modules for your application.
     *
     * @return void
     */
    public function boot()
    {
        // Load folder and start register module
        $directories = array_map('basename', File::directories(__DIR__));
        foreach ($directories as $moduleName) {
            $modulePath = __DIR__ . "/$moduleName/";

            // Boot Route
            if (File::exists($modulePath . "routes/web.php")) {
                $this->loadRoutesFrom($modulePath . "routes/web.php");
            }

            // Boot Migrations
            if (File::exists($modulePath . "migrations")) {
                $this->loadMigrationsFrom($modulePath . "migrations");
            }

            // Boot languages
            // @lang('Demo::general.hello') or {{ __('Demo::general.hello') }}
            if (File::exists($modulePath . "resources/lang")) {
                // Lang with php file
                $this->loadTranslationsFrom($modulePath . "resources/lang", $moduleName);
                // Lang with json file
                $this->loadJSONTranslationsFrom($modulePath . 'resources/lang');
            }

            // Boot views
            // view('Demo::index'), @extends('Demo::index'), @include('Demo::index')
            if (File::exists($modulePath . "resources/views")) {
                $this->loadViewsFrom($modulePath . "resources/views", $moduleName);
            }

            // Boot helpers
            if (File::exists($modulePath . "helpers")) {
                // All files at helpers folder
                $helper_dir = File::allFiles($modulePath . "helpers");
                // Require helpers
                foreach ($helper_dir as $key => $value) {
                    $file = $value->getPathName();
                    require $file;
                }
            }
        }
    }
}